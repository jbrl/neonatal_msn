# neonatal_MSN

Code to reproduce the experiments presented in: 

> Paola Galdi, Manuel Blesa, David Q Stoye, Gemma Sullivan, Gillian J Lamb, Alan J Quigley, Michael J Thrippleton, Mark E Bastin, James P Boardman
> Neonatal morphometric similarity mapping for predicting brain age and characterizing neuroanatomic variation associated with preterm birth. 
> https://doi.org/10.1016/j.nicl.2020.102195

Author: Paola Galdi (paola.galdi@gmail.com)

### Files:
- **MSN_age.ipynb**: ipython notebook reproducing age prediction from neonatal MSNs.
- **MSN_classifier.ipynb**: ipython notebook reproducing preamturity classification from neonatal MSNs.
- **MSN_perms.py**: python script used to run permutation testing for the age prediction task.
- **MSN_cl_perms.py**: python script used to run permutation testing for the classification task.

