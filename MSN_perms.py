import time
import numpy as np
import scipy.io as sio
import pandas as pd
import os
import os.path as op
from scipy import stats
import pickle
import operator

from sklearn import model_selection
from sklearn.linear_model import ElasticNetCV, LinearRegression
from sklearn.preprocessing import RobustScaler
from scipy.spatial.distance import pdist, squareform
from sklearn.svm import SVR
from sklearn.feature_selection import RFECV
from sklearn import feature_selection
from sklearn.model_selection import GridSearchCV
from sklearn.ensemble import RandomForestRegressor
from numpy.random import choice

from multiprocessing import Pool

import os
os.environ['MKL_NUM_THREADS'] = '1'
os.environ['OMP_NUM_THREADS'] = '1'
os.environ['MKL_DYNAMIC'] = 'FALSE'

from itertools import chain, combinations

def all_subsets(ss):
    return chain(*map(lambda x: combinations(ss, x), range(0, len(ss)+1)))

def predict_loo(args,model='elnet',doFilter=False, filterThr=0.01):
    fold, edges, score = args
    n_edges = edges.shape[1]
    train_index, test_index = fold
    k=3 # no. of splits for cross-validation
    if doFilter:
           pears  = [stats.pearsonr(np.squeeze(edges[train_index,j]),score[train_index]) for j in range(0,n_edges)]
           idx_filtered = np.array([idx for idx in range(n_edges) if pears[idx][1]<filterThr])
    else:
        idx_filtered = np.arange(n_edges)
    X_train, X_test, y_train, y_test = edges[np.ix_(train_index,idx_filtered)], edges[np.ix_(test_index,idx_filtered)],score[train_index],score[test_index]
    #f = open("log{}.txt".format(np.random.randint(100)),"a+")
    #f.write("{},{},{}\r\n".format(test_index, X_train.shape,X_test.shape))
    #f.close()
    rbX = RobustScaler()
    X_train = rbX.fit_transform(X_train)
    X_test = rbX.transform(X_test)
    hist_cv, bin_limits_cv = np.histogram(y_train, bins=np.percentile(score,[0,25,50,75,100]))
    bins_cv = np.digitize(y_train, bin_limits_cv[:-1])
    cv = model_selection.StratifiedKFold(n_splits=k)
    if model=='elnet':
        elnet = ElasticNetCV(l1_ratio=[.01, .05, .1, .9, .95, .99],cv=cv.split(X_train, bins_cv),n_jobs=1,max_iter=12000)
        elnet.fit(X_train,y_train)
        predictions = elnet.predict(X_test)
        errors = predictions-score[test_index]
        fscores = elnet.coef_
        params = elnet.l1_ratio_

    return (predictions, errors, fold, fscores, params, idx_filtered)

def predict_parallel(matrices, score, confounds=None, model='elnet', 
              num_cores=8):

    n_parcels = matrices.shape[0]
    triu_idx = np.triu_indices(n_parcels,1)
    n_edges = len(triu_idx[1])
    n_subj = matrices.shape[-1]
    edges = np.zeros([n_subj,n_edges])
    for subject in range(matrices.shape[-1]):
        edges[subject,] = matrices[:,:,subject][triu_idx]
    if not confounds is None:
        edges = np.hstack([edges, confounds])
    folds = model_selection.LeaveOneOut().split(edges)
    args = [(fold,edges,score) for fold in folds]
    with Pool(num_cores,maxtasksperchild=1) as p:
        results = p.map(predict_loo,args,chunksize=1)
        p.close()
    return results


def build_matrices(roi_ids,subset,ids,path):
    msn_mats = np.zeros([len(roi_ids), len(roi_ids),len(ids)])
    i=0
    if len(subset)==1:
        for el in ids:
            fmat = stats.zscore(np.genfromtxt('{}{}_All_parameters_nospaces.csv'.format(path,el),delimiter=','),
                                axis=0,ddof=1)
            msn_mats[:,:,i] = squareform(pdist(fmat[np.ix_(roi_ids,subset)],'minkowski',p=1))
            i=i+1
        return msn_mats
    for el in ids:
        fmat = stats.zscore(np.genfromtxt('{}{}_All_parameters_nospaces.csv'.format(path,el),delimiter=','),
                            axis=0,ddof=1)
        msn_mats[:,:,i] = np.corrcoef(fmat[np.ix_(roi_ids,subset)])
        i=i+1
    return msn_mats

def fsubset(fnames):
    return [col_idx[x] for x in fnames]

def save_results(path,code,score,predictions,errs,folds,fscores,params,roi_ids,features,subjects,model,doFilter,cv):
    results = {'preds': predictions,
               'errs': errs,
               'folds': folds,
               'params': params,
               'rois': roi_ids,
               'features': features,
               'score': score,
               'fscores':fscores
              }
    sio.savemat('{}{}_{}_{}_{}.mat'.format(path, model, 'filter' if doFilter else 'nofilter', cv, code), results)


# ### Declare parameters

datapath = '/home/mblesa/EBC/projects/MSN-ageprediction/data/final-parameters/'
#volume T1/T2 FA MD AD RD KURT NDI(ICVF) ISO ODItot ODIp ODIs
features_all = np.array([
            'Volume', #
            'T1/T2',  #
            'FA',     # fractional anisotropy
            'MD',     # mean diffusivity
            'AD',     # axial diffusivity
            'RD',     # radial diffusivity
            'KURT',   # kurtosis
            'ICVF',   # intra-cellular volume fraction
            'ISO',    # isotropic water volume (CSF)
            'ODItot', # orientation dispersion index (total)
            'ODIp',   # orientation dispersion index (p-axis)
            'ODIs',   # orientation dispersion index (s-axis)
])

best_all_dc = ['Volume', 'FA', 'MD', 'AD', 'KURT', 'ISO', 'ODIp']

# match feature with column index in CSV file
col_idx = {
        'Volume':0,   #
        'T1/T2' :1,   #
        'FA'    :2,   # fractional anisotropy
        'MD'    :3,   # mean diffusivity
        'AD'    :4,   # axial diffusivity
        'RD'    :5,   # radial diffusivity
        'KURT'  :6,   # kurtosis
        'ICVF'  :7,   # intra-cellular volume fraction
        'ISO'   :8,   # isotropic water volume (CSF)
        'ODItot':9,   # orientation dispersion index (total)
        'ODIp'  :10,  # orientation dispersion index (p-axis)
        'ODIs'  :11,  # orientation dispersion index (s-axis)
}

roi_labels = np.array(pd.read_csv('roi_labels.txt',sep='\t', header=None, usecols=[1]))
csf_ids = np.array([49, 50, 83, 84, 85])-1
cc_id = [47]
roi_toremove = np.union1d(csf_ids, cc_id)
wmgm_ids = np.setdiff1d(np.arange(len(roi_labels)),roi_toremove)
wmgm_labels = roi_labels[wmgm_ids]

# TEBC data
demo = pd.read_csv('demo.csv')
ids = np.array(demo['ID'])
GA_all = np.array(demo['exact GAS'])
BA_all = np.array(demo['GA birth'])
gender = np.array(demo['Gender (M/F)']=='m').astype(int) # 1->M, 0->F

term_ids = demo.index[demo['Premature (1/0)']==0]
preterm_ids = demo.index[demo['Premature (1/0)']==1]
labels = np.array(demo['Premature (1/0)']) # 1->PRETERM, 0->TERM
print('All: {} subjects, {} preterm, {} term'.format(len(labels),len(preterm_ids),len(term_ids)))


# ### Permutation testing

def permute_y(y,repetitions,overwrite, datapath=''):
    if not op.isfile(datapath+'permuted{}.txt'.format(repetitions)) or overwrite:
        perm_y = np.vstack([np.random.permutation(y) for i in range(repetitions)])
        np.savetxt(datapath+'permuted{}.txt'.format(repetitions), perm_y)
    else:
        perm_y = np.loadtxt(datapath+'permuted{}.txt'.format(repetitions),dtype=np.int16)
    return perm_y

repetitions = 1000
overwrite = False
permuted_age = permute_y(GA_all,repetitions,overwrite,datapath=datapath)
inputs = range(repetitions)
num_cores = 16

if __name__ == '__main__':
    print(num_cores)
    start = time.time()
    for i in range(repetitions):
        if not op.isfile("perms/perm_age_105s_boot{}.p".format(i)):
            results = predict_parallel(build_matrices(wmgm_ids, fsubset(best_all_dc), ids, datapath),
                permuted_age[i,:],num_cores=num_cores,confounds=np.array((BA_all,gender)).T)
            pickle.dump(results, open("perms/perm_age_105s_boot{}.p".format(i), "wb" ))
    end = time.time()
    print(end - start)

